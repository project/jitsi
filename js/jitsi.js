(function ($, Drupal, drupalSettings) {

    Drupal.behaviors.ek_jitsi = {
        attach: function (context, settings) {
            var logger = [];

            function ek_jitsi_start() {
                var videodomain = settings.ek_jitsi.domain;
                var options = {
                    roomName: settings.ek_jitsi.room,
                    width: '100%',
                    height: '100%',
                    parentNode: document.querySelector('#meet'),
                    userInfo: {
                        email: settings.ek_jitsi.email,
                        displayName: settings.ek_jitsi.user ? settings.ek_jitsi.user : 'guest',
                    },
                    interfaceConfigOverwrite: {enableClosePage: true},
                    configOverwrite: {
                        prejoinPageEnabled: false,
                    },
                };

                if (settings.ek_jitsi.jwt) {
                    $.ajax({
                        url: settings.ek_jitsi.jwt + '?room=' + settings.ek_jitsi.room,
                        async: false,
                        method: 'GET',
                        dataType: "json",
                        success: function (result) {
                            options.jwt = result.token;
                        }
                    });
                }

                api = new JitsiMeetExternalAPI(videodomain, options);
                api.executeCommand('sendTones', {
                    tones: '192837#',
                });

                api.executeCommand('avatarUrl', settings.ek_jitsi.avatar);

                api.on("videoConferenceJoined", function (element) {
                    log_event('Conference Joined', element);
                });

                api.on("videoConferenceLeft", function (element) {
                    log_event('Conference Left', element);
                });

                api.on("participantJoined", function (element) {
                    log_event('Participant Joined', element);
                });

                api.on("participantKickedOut", function (element) {
                    log_event('Participant Kicked out', element);
                })

                api.on("participantLeft", function (element) {
                    log_event('Participant left', element);
                });

                $('#meet-stop', context).show();
                $('#meet-start', context).hide();
                if(settings.ek_jitsi.menu_bar) {
                    $('#on-meet-controls', context).show(500);
                    $('#off-meet-controls', context).show(500);
                } 
            }

            $('#meet-stop', context).hide();
            $('#on-meet-controls', context).hide();
            $('#off-meet-controls', context).hide();
            $('#meet-start', context).click(function (event) {
                event.preventDefault();
                ek_jitsi_start();
            });

            $('#meet-stop', context).click(function (event) {
                event.preventDefault();
                $('#meet-start').show();
                $('#meet-stop').hide();
                $('#on-meet-controls').hide(300);
                api.dispose();
            });

            $('#set-name', context).click(function (event) {
                event.preventDefault();
                var name = $('#staticName').val();
                api.executeCommand('displayName', name);
            });

            $('#set-subject', context).click(function (event) {
                event.preventDefault();
                var subject = $('#staticSubject').val();
                api.executeCommand('subject', subject);
            });

            $('#set-password', context).click(function (event) {
                event.preventDefault();
                var password = $('#staticPassword').val();
                api.executeCommand('password', password);
            });

            $('#share-screen', context).click(function (event) {
                event.preventDefault();
                api.executeCommand('toggleShareScreen');
            });

            $('#meet-chat', context).click(function (event) {
                event.preventDefault();
                api.executeCommand('toggleChat');
                $('#meet-chat span').toggleClass('meetred');
            });

            $('#meet-mute', context).click(function (event) {
                event.preventDefault();
                api.executeCommand('toggleAudio');
                $('#meet-mute span').toggleClass('meetred');
            });

            $('#meet-film', context).click(function (event) {
                event.preventDefault();
                api.executeCommand('toggleFilmStrip');
                $('#meet-film span').toggleClass('meetred');
            });

            $(".copylink", context).click(function () {
                var roomlink = $('#roomlink').attr('href');
                $body = document.getElementsByTagName('body')[0];
                var $tempInput = document.createElement('INPUT');
                $body.appendChild($tempInput);
                $tempInput.setAttribute('value', roomlink)
                $tempInput.select();
                document.execCommand('copy');
                $body.removeChild($tempInput);
            });

            function log_event(event, element) {
                var d = new Date();
                var t = d.getTime();
                var data = [{
                        'type': event,
                        'time': t,
                        'element': element,
                    }]
                logger.push(data);
                console.log(data);
            }

            if (settings.ek_jitsi.autostart == "1") {
                // prevent multiple calls to start video
                settings.ek_jitsi.autostart = 0;
                
                if (settings.ek_jitsi.countdown > 0) {
                    var localdate = new Date(settings.ek_jitsi.timezone);                    
                    // Update the count down every 1 second
                    var x = setInterval(function () {
                        // Get today's date and time
                        var now = new Date().getTime();
                        // Find the distance between now and the count down date
                        var distance = (1000*settings.ek_jitsi.countdown) - now; 
                        // Time calculations for days, hours, minutes and seconds
                        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                        // Display the result in the element 
                        $('#timestamp').html(Drupal.t('Starts in')+ ": "  
                                + days +" "+ Drupal.t('day(s)') +" "
                                + hours +" "+ Drupal.t('hour(s)')+" "
                                + minutes + "m " 
                                + seconds + "s "
                                + "<br>" + localdate);
                        // If the count down is finished go to start
                        if (distance < 0) {
                            clearInterval(x);
                            $('#timestamp').remove();
                            ek_jitsi_start();
                        }
                    }, 1000);
                } else {
                    $('#timestamp').remove();
                    ek_jitsi_start();
                }

            }
        }
    };
})(jQuery, Drupal, drupalSettings);


