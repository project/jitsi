(function ($, Drupal, drupalSettings) {

    Drupal.behaviors.jitsiblock = {
        attach: function (context, settings) {

            $(once('bind-click-event', '#jitsi_new_meeting', context)).on('click', function (event) {
                window.open("/jitsi/start", "", 'width=' + getW(), 'height=' + $(window).height());
            });

            $(once('bind-click-event', '#joinroom', context)).on('click', function (event) {
                var room = $('#roomname').val();
                if (room == '') {
                    alert(Drupal.t('You must enter a room name'));
                } else {
                    window.open("/jitsi/join/" + room, "", 'width=' + getW(), 'height=' + $(window).height());
                }
            });

            function getW() {
                var w = settings.ek_jitsi_block.width;
                return  w;
            }
            

        } 
    }; 
})(jQuery, Drupal, drupalSettings);


