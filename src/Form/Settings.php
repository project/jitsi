<?php

namespace Drupal\ek_jitsi\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure ek_jitsi settings for this site.
 */
class Settings extends ConfigFormBase {

    /**
     * Settings.
     *
     * @var string Config settings
     */
    const SETTINGS = 'ek_jitsi.settings';

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'ek_jitsi_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            static::SETTINGS,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config(static::SETTINGS);

        $form['ek_jitsi_domain'] = [
            '#type' => 'textfield',
            '#required' => TRUE,
            '#title' => $this->t('Signal server'),
            '#attributes' => ['placeholder' => $this->t('domain')],
            '#default_value' => $config->get('ek_jitsi_domain') ? $config->get('ek_jitsi_domain') : 'meet.jit.si',
            '#description' => $this->t('Server that manage video connections'),
        ];

        $form['ek_jitsi_library'] = [
            '#type' => 'radios',
            '#title' => $this->t('Script'),
            '#options' => ['default' => 'meet.jit.si', 'other' => $this->t('other')],
            '#default_value' => $config->get('ek_jitsi_library') ? $config->get('ek_jitsi_library') : 'default',
            '#description' => $this->t('Server that host Jisti script'),
        ];

        $form['ek_jitsi_library_url'] = array(
            '#type' => 'textfield',
            '#default_value' => $config->get('ek_jitsi_library_url') ? $config->get('ek_jitsi_library_url') : '',
            '#attributes' => array('placeholder' => 'sub-domain.domain'),
            '#description' => $this->t('i.e. "api.example.com"'),
            '#states' => array(
                'visible' => array(":input[name='ek_jitsi_library']" => ['value' => 'other']),
                'required' => array(":input[name='ek_jitsi_library']" => ['value' => 'other']),
            ),
        );

        $form['ek_jitsi_view'] = [
            '#type' => 'select',
            '#title' => $this->t('Jitsi view mode'),
            '#required' => TRUE,
            '#options' => [
                'full' => $this->t("Full screen"),
                'framed' => $this->t("Framed"),
            ],
            '#size' => 1,
            '#default_value' => $config->get('ek_jitsi_view') ? $config->get('ek_jitsi_view') : 'full',
            '#description' => $this->t('Display full screen or framed into your site layout'),
        ];

        $form['ek_jitsi_window_width'] = [
            '#type' => 'number',
            '#min' => 0,
            '#max' => 3000,
            '#title' => $this->t('Jitsi window width'),
            '#required' => TRUE,
            '#default_value' => $config->get('ek_jitsi_window_width') ? $config->get('ek_jitsi_window_width') : 800,
            '#description' => $this->t('size of popup window'),
        ];

        $form['ek_jitsi_jwt'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Jitsi JWT Provider'),
            '#default_value' => $config->get('ek_jitsi_jwt') ? $config->get('ek_jitsi_jwt') : '',
            '#description' => $this->t('URL of the JWT provider end point.'),
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

        $this->configFactory->getEditable(static::SETTINGS)
                ->set('ek_jitsi_domain', $form_state->getValue('ek_jitsi_domain'))
                ->set('ek_jitsi_library', $form_state->getValue('ek_jitsi_library'))
                ->set('ek_jitsi_library_url', $form_state->getValue('ek_jitsi_library_url'))
                ->set('ek_jitsi_view', $form_state->getValue('ek_jitsi_view'))
                ->set('ek_jitsi_window_width', $form_state->getValue('ek_jitsi_window_width'))
                ->set('ek_jitsi_jwt', $form_state->getValue('ek_jitsi_jwt'))
                ->save();

        parent::submitForm($form, $form_state);
    }

}
