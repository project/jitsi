<?php

namespace Drupal\ek_jitsi\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Plugin implementation of the 'ek_jitsi_conference_default' formatter.
 *
 * @FieldFormatter(
 *   id = "ek_jitsi_conference_default",
 *   label = @Translation("Default"),
 *   field_types = {"ek_jitsi_conference"}
 * )
 */
class ConferenceDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options = parent::defaultSettings();

    $options['domain'] = 'meet.jit.si';
    $options['rlink'] = false;
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $settings = $this->getSettings();
    $form['domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('domain'),
      '#default_value' => $settings['domain'] ? $settings['domain'] : '',
    ];
    $form['rlink'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('display room link'),
      '#default_value' => $settings['rlink'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $d = ($settings['domain'] != '') ? $settings['domain'] : $this->t('default');
    $summary[] = $this->t('domain: @domain', ['@domain' => $d]);
    $l = ($settings['rlink'] == 1) ? $this->t('yes') : $this->t('no');
    $summary[] = $this->t('display room link: @link', ['@link' => $l]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $settings = $this->getSettings();    
    $config = \Drupal::config('ek_jitsi.settings');
    // default domain is from main configuration
    $domain = $config->get('ek_jitsi_domain');
    $settings = $this->getSettings();
    if($settings['domain'] != '') {
        // override the main configuration
        $domain = $settings['domain'];
    }
    foreach ($items as $delta => $item) {
      if ($item->jitsi_conf) {
        $element[$delta]['jitsi_conf'] = $this->join($item->jitsi_conf,$domain,$settings['rlink']);
      }
    }
    return $element;
  }

  /**
   * Function to join video call.
   *
   * @param string $room
   *   Key room.
   *
   * @return array
   *   Return Array.
   */
  public function join($room, $domain,$room_link) {
      
    if (!$room) {
      throw new NotFoundHttpException();
    }

    // set default picture
    $theme = theme_get_setting('logo');
    $picture = \Drupal::request()->getSchemeAndHttpHost() . $theme['url'];
    $mail = "";
    if(!\Drupal::currentUser()->isAuthenticated()) {
        $username = $this->t('Guest');
    } else {
        $user = User::load(\Drupal::currentUser()->id());
        $username = $user->getAccountName();
        $mail = $user->getEmail();
        // set custom picture for logged user
        if ($user->get('user_picture')->entity) {
          $picture = \Drupal::service('file_url_generator')->generateAbsoluteString($user->get('user_picture')->entity->getFileUri());
        }
    }    

    // The room share link point to the video.
    $link_external = null;
    if($room_link){
        $url = Url::fromUri('https://' . $domain . '/' . $room, ['attributes' => ['id' => 'roomlink', 'title' => $this->t('Right click to copy')]]);
        $link_external = Link::fromTextAndUrl('Room link', $url);
    }
    $content = [
      '#theme' => 'jitsi_video_field_join',
      '#room' => $room,
      '#user' => $username,
      '#link_external' => $link_external,
      '#attached' => [
        'drupalSettings' => [
          'ek_jitsi' => [
            'autostart' => 0,
            'domain' => $domain,
            'room' => $room,
            'user' => $username,
            'email' => $mail,
            'avatar' => $picture,
          ],
        ],
        'library' => ['ek_jitsi/video'],
      ],
    ];
    return $content;

  }
}
