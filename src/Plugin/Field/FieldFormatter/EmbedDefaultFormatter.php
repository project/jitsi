<?php

namespace Drupal\ek_jitsi\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Plugin implementation of the 'ek_jitsi_embed_default' formatter.
 *
 * @FieldFormatter(
 *   id = "ek_jitsi_embed_default",
 *   label = @Translation("Default"),
 *   field_types = {"ek_jitsi_embed"}
 * )
 */
class EmbedDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options = parent::defaultSettings();
    $options['domain'] = 'meet.jit.si';
    $options['rlink'] = false;
    $options['menu_bar'] = true;
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $settings = $this->getSettings();
    $form['domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('domain'),
      '#default_value' => $settings['domain'] ? $settings['domain'] : '',
    ];
    $form['rlink'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('display room link'),
      '#default_value' => $settings['rlink'],
    ];
    $form['menu_bar'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('display menu bar'),
      '#default_value' => $settings['menu_bar'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $d = ($settings['domain'] != '') ? $settings['domain'] : $this->t('default');
    $summary[] = $this->t('domain: @domain', ['@domain' => $d]);
    $l = ($settings['rlink'] == 1) ? $this->t('yes') : $this->t('no');
    $summary[] = $this->t('display room link: @link', ['@link' => $l]);
    $l = ($settings['menu_bar'] == 1) ? $this->t('yes') : $this->t('no');
    $summary[] = $this->t('display menus: @m', ['@m' => $l]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    $route_name = \Drupal::routeMatch()->getRouteName();
        if ($route_name == 'entity.node.canonical') {
          $entity = \Drupal::routeMatch()->getParameter('node');
        } elseif ($route_name == 'entity.node.preview') {
          $entity = \Drupal::routeMatch()->getParameter('node_preview');
        }
    if(isset($entity)) {    
        $alias = \Drupal::service('path_alias.manager')->getAliasByPath('/node/'. $entity->id(), $langcode);
            if($alias != '/node/'. $entity->id()) {
                // use alias for room name
                $room = str_ireplace(" ","-",$alias);
                $room = ltrim($room, '/');
            } else {
                $room = str_ireplace(" ","-",$entity->getTitle());
            }
        $param = [];
        $config = \Drupal::config('ek_jitsi.settings');
        // default domain is from main configuration
        $param['domain'] = $config->get('ek_jitsi_domain');
        $settings = $this->getSettings();
        $param['rlink'] = $settings['rlink'];
        $param['menu_bar'] = $settings['menu_bar'];
        if($settings['domain'] != '') {
            // override the main configuration
            $param['domain'] = $settings['domain'];
        } 
        $param['countdown'] = 0;
        $param['timezone'] = '';
        if($entity->hasField('field_timestamp')) {
            $target = $entity->get('field_timestamp')->value;
            if($target > time()) {
                $param['countdown'] = $target;
                $t1 = new \DateTime(date('Y-m-d h:i:s', $target));
                $param['timezone'] = $t1->format("D M d Y H:i:s O (e)");
            }


        }
        foreach ($items as $delta => $item) {
          $elements[$delta] = $this->join($room, $param);
        }
    }
    return $elements;
  }

  /**
   * Function to join video call.
   *
   * @param string $room
   *   Key room.
   *
   * @return array
   *   Return Array.
   */
  public function join($room, $param) {
    if (!$room) {
      throw new NotFoundHttpException();
    }
    // set default picture
    $theme = theme_get_setting('logo');
    $picture = \Drupal::request()->getSchemeAndHttpHost() . $theme['url'];
    $mail = "";
    if(!\Drupal::currentUser()->isAuthenticated()) {
        $username = $this->t('Guest');
    } else {
        $user = User::load(\Drupal::currentUser()->id());
        $username = $user->getAccountName();
        $mail = $user->getEmail();
        // set custom picture for logged user
        if ($user->get('user_picture')->entity) {
          $picture = \Drupal::service('file_url_generator')->generateAbsoluteString($user->get('user_picture')->entity->getFileUri());
        }
    }    

    // The room share link point to the video.
    $link_external = null;
    if($param['rlink']){
        // field settings requires link
        $url = Url::fromUri('https://' . $param['domain'] . '/' . $room, ['attributes' => ['id' => 'roomlink', 'title' => $this->t('Right click to copy')]]);
        $link_external = Link::fromTextAndUrl('Room link', $url);
    }
    $content = [
      '#theme' => 'jitsi_video_field_embed',
      '#room' => $room,
      '#user' => $username,
      '#link_external' => $link_external,
      '#attached' => [
        'drupalSettings' => [
          'ek_jitsi' => [
            'autostart' => 1,
            'countdown' => $param['countdown'],
            'timezone' => $param['timezone'],
            'domain' => $param['domain'],
            'room' => $room,
            'user' => $username,
            'email' => $mail,
            'avatar' => $picture,
            'menu_bar' => $param['menu_bar'],
          ],
        ],
        'library' => ['ek_jitsi/video'],
      ],
      '#cache' => [
          'max-age' => 60,
      ],
    ];
    return $content;

  }
}
