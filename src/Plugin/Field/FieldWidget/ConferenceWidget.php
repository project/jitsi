<?php

namespace Drupal\ek_jitsi\Plugin\Field\FieldWidget;


use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Defines the 'ek_jitsi_conference' field widget.
 *
 * @FieldWidget(
 *   id = "ek_jitsi_conference",
 *   label = @Translation("jitsi conference"),
 *   field_types = {"ek_jitsi_conference"},
 * )
 */
class ConferenceWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['jitsi_conf'] = [
      '#type' => 'textfield',
      '#title' => $this->t('jitsi conf'),
      '#default_value' => isset($items[$delta]->jitsi_conf) ? $items[$delta]->jitsi_conf : str_replace('-','',$this->fieldDefinition->getUniqueIdentifier()),
      '#size' => 20,
    ];

    $element['#theme_wrappers'] = ['container', 'form_element'];
    $element['#attributes']['class'][] = 'container-inline';
    $element['#attributes']['class'][] = 'ek-jitsi-conference-elements';
    // $element['#attached']['library'][] = 'ek_jitsi/ek_jitsi_conference';
    $element['#attached']['library'][] = 'ek_jitsi/video';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    return isset($violation->arrayPropertyPath[0]) ? $element[$violation->arrayPropertyPath[0]] : $element;
  }
}
