<?php

namespace Drupal\ek_jitsi\Plugin\Field\FieldWidget;


use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Defines the 'ek_jitsi_embed' field widget.
 *
 * @FieldWidget(
 *   id = "ek_jitsi_embed",
 *   label = @Translation("jitsi embed"),
 *   field_types = {"ek_jitsi_embed"},
 * )
 */
class EmbedWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['jitsi_embed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Insert jitsi video on page'),
      '#default_value' => 1,
    ];

    $element['#theme_wrappers'] = ['container', 'form_element'];
    $element['#attributes']['class'][] = 'container-inline';
    $element['#attributes']['class'][] = 'ek-jitsi-embed-elements';
    $element['#attached']['library'][] = 'ek_jitsi/video';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    return isset($violation->arrayPropertyPath[0]) ? $element[$violation->arrayPropertyPath[0]] : $element;
  }
}
