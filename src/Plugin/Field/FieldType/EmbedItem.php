<?php

namespace Drupal\ek_jitsi\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the 'ek_jitsi_embed' field type.
 *
 * @FieldType(
 *   id = "ek_jitsi_embed",
 *   label = @Translation("Jitsi Embed Field"),
 *   description = @Translation("Field for creating jitsi embed video"),
 *   category = @Translation("EK Jitsi Field"),
 *   default_widget = "ek_jitsi_embed",
 *   default_formatter = "ek_jitsi_embed_default"
 * )
 */
class EmbedItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['jitsi_embed'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('jitsi embeded video'));

    return $properties;
  }
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    return [
      'columns' => [
        'jitsi_embed' => [
          'type' => 'int',
          'size' => 'tiny',
        ],
      ],
    ];
  }

  
  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    
    $value = $this->getValue();
    if (isset($value['jitsi_embed']) && $value['jitsi_embed'] != '') {
      return FALSE;
    }
    return TRUE;
    
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();
    return $constraints;
  }



}
