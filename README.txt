"EK Jitsi"

## usage

After module is installed, go to [configuration] (/admin/ek_jitsi).
Enter the DNS for the service.
Set other options and save.

There are 2 types of fields that can be added to a node: 
- conference : full video interface where you can set the room name;
- embed : autostart video where room name is base on node alias or title;
To add a field to a node, go to  [Structure] (/admin/structure) then [Content Type] select the type you want to have and add a new field select (conference, embed) type and configure jitsi server domain.
The server setting in the field will override the default module configuration.
You should be able to add field to Users, Taxonomies and other entities the same way.
Upon installation a new content type "video" is created with default embed video field.
To enable the field, you need to go to [Structure] (/admin/structure/types/manage/video/form-display).
Once field is enabled, you can create new video content.
There is an option to display a countdown with embed video field.
To do that, add a new field of type "timestamp". The field label must be set to "timestamp" in order to be recognized by the module.
When you create a video page content, you can then set the time for the video to start (i.e 1 hour later) and the module will set countdown until video launches.


Go to [Structure] (/admin/structure) then [Block layout]
(/admin/structure/block) to install the Jitsi block.

Once the block is installed, click on New Meeting or Join room
if you have a room name.
Start video.
